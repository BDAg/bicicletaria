<div align="center"><h1>Auto-Bike</h1></div>
<pre>O projeto visa automatizar processos presentes em uma bicicletaria, cujo problema é a desordem administrativa, 
aumentando assim a praticidade e a eficiência da execução das tarefas ali presentes e possibilitando ao cliente usufruir
de novas experiências com um serviço de gestão que oferece inúmeras inovações..</pre>
<hr/>

### Wiki
[Wiki do projeto](https://gitlab.com/BDAg/bicicletaria/-/wikis/home)
<hr/>

### Equipe

[Daniel Vieira Mendes](https://gitlab.com/BDAg/management-bike/app-web/wikis/Daniel-Vieira-Mendes)
<br>
[Helena Mattos Sotrate](https://gitlab.com/BDAg/management-bike/app-web/wikis/Helena-Mattos-Sotrate)
<br>
[Iago Nogueira Medeiros](https://gitlab.com/BDAg/management-bike/app-web/wikis/Iago-Nogueira-Medeiros)
<br>
[Janaina Pinheiro Gonçalez](https://gitlab.com/BDAg/management-bike/app-web/wikis/Janaina-Pinheiro-Gon%C3%A7alez)
<br>
[Luis Fernando Kameda Carvajhal](https://gitlab.com/BDAg/management-bike/app-web/wikis/Luis-Fernando-Kameda-Carvajhal)
<br>
[Renan Rodrigues Ferreira](https://gitlab.com/BDAg/management-bike/app-web/wikis/Renan-Rodrigues-Ferreira)
<hr/>


### Tutoriais

[Instalação Node.js e React.js](./Instalacao-Node-React)<br/>
[Como usar nossos códigos React](./Usar-codigo-React)
